# Unity

#### 介绍

本人学习制作一些项目代码

#### 内容说明

0. Note - unity相关笔记

1. Hit the bricks - 打砖块 3d (已完成)

2. Tank - 坦克大战 2d (已完成)

3. Pacman - 吃豆人 2d (已完成)

4. Tower Defense - 塔防小游戏 3d (已完成)

5. FlashMaster - 捕鱼达人 2d (已完成)

6. Chru - 自制游戏 3d探索类 (半成品)

7. Moba - 自制游戏 2.5d竞技类 (半成品)

8. Baba is you - 同名重置版解谜游戏(已完成30关)


//注:所有项目止步于2021.12.31

