本文件夹内的文件夹信息

| 文件夹名字       | 开发者        | 开发版本 | 详情说明                         |
| ---------------- | ------------- | -------- | -------------------------------- |
| Bate 0           | Github@sejinP | 0        | Github作者sejinP源文件           |
| cz over Bate 0   | Github@sejinP | 0        | Github作者sejinP运作试用版本     |
| Bate 1.0         | Gitee@Chru    | 1.0      | 第一次重构时设计地图版本的脚本   |
| Bate 1.1         | Gitee@Chru    | 1.1      | 重构后可以运作的最新版脚本       |
| cz over Bate 1.1 | Gitee@Chru    | 1.1      | Bate 1.1脚本展现的可运作试用版本 |

0 -> 1.0

重构了载体的特性存储方式(载体自身特性->全局变量)

1.0 -> 1.1

重构了字典型的规则判定方式

重构了地图存储的方式(LevelCreator.cs -> MapMaker.cs)

重构了规则判定的代码结构(各项指标检测 名字Check以if分割代码块)



注:本gitee库开发者是Gitee@Chru 并且不认识Github@sejinP 本人制作仅以展现unity游戏功能 若侵权请联系删除
